# Projet_EntityFramework

Ce projet est un un travail concernant le jeu vidéo League of Legends. Il consistait à faire une api et utiliser un Framework en .NET pour gérer une base de données.

La dernière version du projet est sur la branche testApp car je voulais essayer de le relier à l'application Maui.

Le schéma d'architecture se trouve dans le dossier documentation de la branche master.

## Partie Entity Framework:

Actuellement, il y a les champions et les larges images qui sont implémentées cependant lorsque j'ai voulu refaire une migration pour ajouter les larges images j'obtenais cette erreur: Unable to create an object of type 'LolContext'. For the different patterns supported at design time, see https://go.microsoft.com/fwlink/?linkid=851728. Je n'ai pas réussi à la régler.

J'ai rajouter des Id dans les classes Champion et LargeImage afin de pouvoir les identifier dans la base de données.

Ensuite, j'ai fais les tests unitaires de la classe DbChampionManager qui hérite de IChampionsManager.

J'ai voulu relier l'application Maui à mon ChampionManager cependant j'obtenais l'erreur suivante quand j'essayais d'ajouter une migration : The specified deps.json [/Users/thomaschazot/Documents/But2A/EntityFramework/Projet_EntityFramework/Sources/LolApp/bin/Debug/net7.0-android/LolApp.deps.json] does not exist

## Partie API

J'ai fais toutes les opérations CRUD des classes champions et runes. J'ai utiliser les logs et commencer les tests unitaires du champion contrroller. J'ai aussi voulou relier la partie api à la partie Entity Framework cependant je n'ai pas pu à cause du problème lors de l'ajoue des migrations, de même pour la liaison à l'application mobile.

Je n'ai pas eu le temps de conteneuriser l'api.