﻿namespace Tests;

using System.Threading.Tasks;
using DataManagers;
using EFLib;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Model;

public class DbChampionManager_UT
{
    [Fact]
    public async Task Add_TestAsync()
    {
        var connection = new SqliteConnection("DataSource=:memory:");
        connection.Open();

        var options = new DbContextOptionsBuilder<LolContext>()
                .UseSqlite(connection)
                .Options;

        using (var db = new DbDataManager(new EFLib.LolContext(options)))
        {
            db.EnsureCreated();
            Champion champion = new Champion("Aurelien", ChampionClass.Tank, "tro bo le type");
            Champion champion2 = new Champion("Croissant", ChampionClass.Assassin, "tro bon le type");
            Champion champion3 = new Champion("Mathilde", ChampionClass.Unknown, "Chut mathilde");
            await db.ChampionsMgr.AddItem(champion);
            await db.ChampionsMgr.AddItem(champion2);
            await db.ChampionsMgr.AddItem(champion3);

        }

        using (var db = new DbDataManager(new EFLib.LolContext(options)))
        {
            db.EnsureCreated();
            Assert.Equal(3, await db.ChampionsMgr.GetNbItems());
        }


    }

    [Fact]
    public async Task Delete_TestAsync()
    {
        var connection = new SqliteConnection("DataSource=:memory:");
        connection.Open();

        var options = new DbContextOptionsBuilder<LolContext>()
                .UseSqlite(connection)
                .Options;

        using (var db = new DbDataManager(new EFLib.LolContext(options)))
        {
            db.EnsureCreated();

            Champion champion = new Champion("Aurelien", ChampionClass.Tank, "tro bo le type");
            Champion champion2 = new Champion("Croissant", ChampionClass.Assassin, "tro bon le type");
            Champion champion3 = new Champion("Mathilde", ChampionClass.Unknown, "Chut mathilde");
            await db.ChampionsMgr.AddItem(champion);
            await db.ChampionsMgr.AddItem(champion2);
            await db.ChampionsMgr.AddItem(champion3);

            Assert.Equal(3, await db.ChampionsMgr.GetNbItems());
        }

        using (var db = new DbDataManager(new EFLib.LolContext(options)))
        {
            db.EnsureCreated();

            string nameToFind = "Croissant";
            IEnumerable<Champion> champ = await db.ChampionsMgr.GetItemsByName(nameToFind, 0, 15, "name");
            Champion c = champ.First();
            Assert.Equal("Croissant", c.Name);
            await db.ChampionsMgr.DeleteItem(champ.First());

            Assert.Equal(2, await db.ChampionsMgr.GetNbItems());
        }
    }

    [Fact]
    public async Task Modify_TestAsync()
    {
        var connection = new SqliteConnection("DataSource=:memory:");
        connection.Open();

        var options = new DbContextOptionsBuilder<LolContext>()
                .UseSqlite(connection)
                .Options;

        using (var db = new DbDataManager(new EFLib.LolContext(options)))
        {
            db.EnsureCreated();

            Champion champion = new Champion("Aurelien", ChampionClass.Tank, "tro bo le type");
            Champion champion2 = new Champion("Croissant", ChampionClass.Assassin, "tro bon le type");
            Champion champion3 = new Champion("Mathilde", ChampionClass.Unknown, "Chut mathilde");
            await db.ChampionsMgr.AddItem(champion);
            await db.ChampionsMgr.AddItem(champion2);
            await db.ChampionsMgr.AddItem(champion3);

            Assert.Equal(3, await db.ChampionsMgr.GetNbItems());
        }

        using (var db = new DbDataManager(new EFLib.LolContext(options)))
        {
            db.EnsureCreated();

            string nameToFind = "Croissant";
            IEnumerable<Champion> champ = await db.ChampionsMgr.GetItemsByName(nameToFind, 0, 15, "name");
            Champion c = champ.First();
            Assert.Equal("Croissant", c.Name);
            Champion c1 = new Champion("Test", c.Class, c.Icon, bio: c.Bio);
            await db.ChampionsMgr.UpdateItem(c, c1);

            Assert.Equal(3, await db.ChampionsMgr.GetNbItems());
            champ = await db.ChampionsMgr.GetItemsByName("Test", 0, 15, "name");

            Assert.Equal("Test", champ.First().Name);
        }
    }
}