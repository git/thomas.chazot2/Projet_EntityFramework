﻿using System;
using ApiLol.Mapping;
using EFLib;
using Model;

namespace ApiLol
{
	public static class ChampionMapper
	{

        public static ChampionDTO ToDto(this Champion champion)
        {
            if (champion == null)
            {
                throw new NullReferenceException();
            }
            return new ChampionDTO
            {
                Name = champion.Name,
                Bio = champion.Bio,
                Icon = champion.Icon,
                ChampClass = champion.Class,
                Image = champion.Image,
                Skins = champion.Skins,
                Characteristics = champion.Characteristics.ToDto()
            };
        }

        public static IEnumerable<ChampionDTO> ToDtos(this IEnumerable<Champion> champions)
        {
            IEnumerable<ChampionDTO> dtos = new List<ChampionDTO>();
            foreach (Champion champion in champions)
            {
                dtos = dtos.Append(champion.ToDto());
            }
            return dtos;
        }

        public static Champion ToPoco(this ChampionDTO champion)
        {
            if (champion == null)
            {
                throw new NullReferenceException();
            }
            return new Champion(name: champion.Name, champClass: champion.ChampClass, icon: champion.Icon, bio: champion.Bio, image: champion.Image.Base64);
        }

        public static IEnumerable<Champion> ToPocos(this IEnumerable<ChampionDTO> champions)
        {
            IEnumerable<Champion> dtos = new List<Champion>();
            foreach (ChampionDTO champion in champions)
            {
                dtos = dtos.Append(champion.ToPoco());
            }
            return dtos;
        }


    }
}

