﻿using System;
using ApiLol.DTO;
using System.Collections.ObjectModel;
using Model;

namespace ApiLol.Mapping
{
	public static class SkinMapper
	{
        public static SkinDTO ToDto(this Skin skin)
        {
            return new SkinDTO
            {
                Name = skin.Name,
                Description = skin.Description,
                Price = skin.Price,
                Icon = skin.Icon,
                Image = skin.Image
            };
        }


        //public static Skin ToPoco(this SkinDTO skin)
        //{
        //    return new Skin(skin.Name, skin.Price, skin.Icon, skin.Image.Base64, skin.Description);
        //}
    }
}

