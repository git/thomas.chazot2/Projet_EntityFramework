﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using ApiLol.DTO;
using Model;

namespace ApiLol.Mapping
{
	public static class CharacteristicMapper
	{
        public static IEnumerable<CharacteristicDTO> ToDto(this ReadOnlyDictionary<string, int> dico)
        {
            if (dico == null)
            {
                throw new ArgumentNullException();
            }

            IEnumerable<CharacteristicDTO> characteristicDTOs = new List<CharacteristicDTO>();

            foreach(var item in dico)
            {
                characteristicDTOs = characteristicDTOs.Append(new CharacteristicDTO() { Name = item.Key, Val = item.Value });
            }
            return characteristicDTOs;
        }


        public static ReadOnlyDictionary<string, int> ToDto(this IEnumerable<CharacteristicDTO> characs)
        {
            if (characs == null)
            {
                throw new ArgumentNullException();
            }

            Dictionary<string, int> dico = new Dictionary<string, int>();
            foreach (CharacteristicDTO c in characs)
            {
                dico.Add(c.Name, c.Val);
            }

            return new ReadOnlyDictionary<string, int>(dico);

        }
    }
}

