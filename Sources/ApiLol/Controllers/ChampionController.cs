﻿using System;
using StubLib;
using EFLib;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Model;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ApiLol.Controllers
{
    [Route("api/[controller]")]
    public class ChampionController : ControllerBase
    {
        private readonly ILogger<ChampionController> _logger;

        private StubData.ChampionsManager championsManager = new StubData.ChampionsManager(new StubData());

        public ChampionController(ILogger<ChampionController> logger)
        {
            _logger = logger;
        }


        // GET: api/champion
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ChampionDTO>>> Get()
        {
            IEnumerable<Champion?> champs = await championsManager.GetItems(0, await championsManager.GetNbItems());
            IEnumerable<ChampionDTO> championDTOs = champs.ToDtos();

            return Ok(championDTOs);
        }

        /*
        // GET api/champion/5
        [HttpGet("{id}")]
        public ActionResult<ChampionDTO?> Get(int id)
        {
            ChampionDTO? champion = champions.SingleOrDefault((ChampionEntity arg) => arg.Id == id)?.ToDto();
            if (champion != null)
            {
                return Ok(champion);
            }
            return BadRequest();
        }
        */


        // POST api/champion
        [HttpPost]
        public ActionResult Post([FromBody]ChampionDTO? champion)
        {
            if (champion == null){
                return BadRequest();
            }
            else
            {
                championsManager.AddItem(champion.ToPoco());
                return Ok();
            }
        }

        // PUT api/champion/5
        [HttpPut("ChampionDto")]
        public ActionResult Put([FromBody] ChampionDTO? champion, ChampionDTO? newChampion) 
        {
            if (champion == null || newChampion == null)
            {
                return BadRequest();
            }
            else
            {
                championsManager.UpdateItem(champion.ToPoco(), newChampion.ToPoco());
                return Ok();
            }
        }

        // DELETE api/values/5
        [HttpDelete("ChampionDto")]
        public ActionResult Delete([FromBody] ChampionDTO? champion)
        {
            if (champion == null)
            {
                return BadRequest();
            }
            else
            {
                championsManager.DeleteItem(champion.ToPoco());
                return Ok();
            }
        }
    }
}

