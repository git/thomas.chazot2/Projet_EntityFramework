﻿using System;
using ApiLol.DTO;
using Model;

namespace ApiLol
{
	public class ChampionDTO
	{
        public int Id { get; set; }

        public string Name { get; set; }

        public string Icon { get; set; }

        public string Bio { get; set; }

        public ChampionClass ChampClass { get; set; }

        public LargeImage Image { get; set; }

        public IEnumerable<CharacteristicDTO> Characteristics { get; set; }

        public IEnumerable<Skin> Skins { get; set; }

    }
}

