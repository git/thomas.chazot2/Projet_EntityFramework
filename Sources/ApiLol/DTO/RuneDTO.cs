﻿using System;
using Model;

namespace ApiLol.DTO
{
	public class RuneDTO
	{
		public string Nom { get; set; }

        public string Description { get; set; }

        public string Famille { get; set; }

        public string Icon { get; set; }

        public string Name { get; set; }

        public LargeImage Image { get; set; }


    }
}

