﻿using System;
using Model;

namespace ApiLol.DTO
{
	public class SkinDTO
	{
        public string Name { get; set; }

        public string Description { get; set; }

        public float Price { get; set; }

        public string Icon { get; set; }

        public LargeImage Image { get; set; }

    }
}

