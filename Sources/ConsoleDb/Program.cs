﻿// See https://aka.ms/new-console-template for more information
using DataManagers;
using Model;

Console.WriteLine("Hello, World!");


using (var db = new DbDataManager(new EFLib.LolContext()))
{
    IEnumerable<Champion?> champions = await db.ChampionsMgr.GetItems(0, 10);

    foreach (Champion c in champions)
    {
        Console.WriteLine(c.Name);
    }
}

