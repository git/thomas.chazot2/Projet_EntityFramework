﻿using System;
using Model;

namespace EFLib
{
	public class ChampionEntity
	{

		public int Id { get; set; }

		public string Name { get; set; }

		public string Icon { get; set; }

		public string Bio { get; set; }

		public ChampionClass ChampClass { get; set;}

    }
}

