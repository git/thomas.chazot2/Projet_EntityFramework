﻿using System;
using Microsoft.EntityFrameworkCore;

namespace EFLib
{
	public class LolContext : DbContext, IDisposable
	{
        public DbSet<ChampionEntity> Champions { get; set; }

        public LolContext(DbContextOptions<LolContext> options)
        : base(options)
        { }

        public LolContext()
        { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlite("Data Source=oiseaux.db");
            }
        }
    }
}

