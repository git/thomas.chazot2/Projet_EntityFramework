﻿// See https://aka.ms/new-console-template for more information
using EFLib;

Console.WriteLine("Hello, World!");

using (var context = new LolContext())
{
    context.Champions.Add(new ChampionEntity
    {
        Name = "Test",
        Bio = "Bonjour",
        Icon = "Wouhou"
    });
    context.SaveChanges();
}