﻿using System;
using EFLib;
using Model;

namespace DataManagers
{
	public class DbDataManager : IDataManager
	{

		private LolContext Context { get; set; }

        public IChampionsManager ChampionsMgr => new DbChampionManager(Context);

        public ISkinsManager SkinsMgr => throw new NotImplementedException();

        public IRunesManager RunesMgr => throw new NotImplementedException();

        public IRunePagesManager RunePagesMgr => throw new NotImplementedException();


        public DbDataManager(LolContext context)
        {
            Context = context;
        }

        public void Dispose()
        {
            Context.Dispose();
        }

        public void EnsureCreated()
        {
            Context.Database.EnsureCreated();
        }
    }
}

