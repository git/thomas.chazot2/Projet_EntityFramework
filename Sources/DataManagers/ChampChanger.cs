﻿using System;
using EFLib;
using Model;

namespace DataManagers
{
	public static class ChampChanger
	{
        public static Champion ToPoco(this ChampionEntity champion)
        {
            return new Champion(id: champion.Id, name: champion.Name, champClass: champion.ChampClass, icon: champion.Icon, bio: champion.Bio);
        }

        public static ChampionEntity ToEntity(this Champion champion) => new ChampionEntity
        {
            Id = champion.Id,
            Name = champion.Name,
            Bio = champion.Bio,
            Icon = champion.Icon,
            ChampClass = champion.Class,
        };

        public static IEnumerable<Champion> ToPocos(this IEnumerable<ChampionEntity> champs)
        {
            List<Champion> champions = new List<Champion>();
            foreach (ChampionEntity c in champs)
            {
                champions.Add(c.ToPoco());
            }
            return champions;
        }

        public static IEnumerable<ChampionEntity> toEntities(this IEnumerable<Champion> champs)
        {
            List<ChampionEntity> champions = new List<ChampionEntity>();
            foreach (Champion c in champs)
            {
                champions.Add(c.ToEntity());
            }
            return champions;
        }
    }
}

