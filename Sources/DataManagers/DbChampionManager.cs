﻿using System;
using Microsoft.EntityFrameworkCore;
using Model;
using EFLib;

namespace DataManagers
{
	public class DbChampionManager : IChampionsManager
	{
        private LolContext Context { get; set; }

		public DbChampionManager(LolContext context)
		{
            Context = context;
		}

        private IEnumerable<Champion> SortChampions(IEnumerable<Champion> champs, int index, int count, string? orderingPropertyName = null, bool descending = false)
        {
            switch (orderingPropertyName?.ToLower())
            {
                case "name":
                    champs = champs.OrderBy(arg => arg.Name).ToList();
                    break;
                case "bio":
                    champs = champs.OrderBy(arg => arg.Bio).ToList();
                    break;
                case "class":
                    champs = champs.OrderBy(arg => arg.Class).ToList();
                    break;
                default:
                    break;

            }
            if (descending)
            {
                champs = champs.Reverse().ToList();
            }
            if (index + count > champs.ToList().Count) return champs.ToList().GetRange(index, champs.ToList().Count - index);
            return champs.ToList().GetRange(index, count);
        }

        public async Task<Champion?> AddItem(Champion? item)
        {
            if (item != null)
            {
                await Context.Champions.AddAsync(item.ToEntity());
                await Context.SaveChangesAsync();
                return await Task.Run(() => Context.Champions.SingleOrDefault((ChampionEntity arg) => arg.Name == item.Name)?.ToPoco());
            }
            return null;
        }

        public async Task<bool> DeleteItem(Champion? item)
        {
            if (item != null)
            {
                ChampionEntity? entity = await Context.Champions.SingleOrDefaultAsync((ChampionEntity arg) => arg.Name == item.Name);
                if (entity != null)
                {
                    Context.Champions.Remove(entity);
                    int test = await Context.SaveChangesAsync();
                    return test==1;
                }
            }
            return false;
        }

        public async Task<IEnumerable<Champion?>> GetItems(int index, int count, string? orderingPropertyName = null, bool descending = false)
        {
            IEnumerable<Champion> champs = await Task.Run(() => Context.Champions.ToList().ToPocos());
            return SortChampions(champs, index, count,orderingPropertyName, descending);
        }

        public async Task<int> GetNbItems()
        {
            return await Task.FromResult(Context.Champions.Count());

        }

        public Task<IEnumerable<Champion?>> GetItemsByCharacteristic(string charName, int index, int count, string? orderingPropertyName = null, bool descending = false)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<Champion?>> GetItemsByClass(ChampionClass championClass, int index, int count, string? orderingPropertyName = null, bool descending = false)
        {
            IEnumerable<Champion> champs = await Task.Run(() => Context.Champions.ToList().ToPocos().Where(arg => arg.Class == championClass));
            return SortChampions(champs, index, count, orderingPropertyName, descending);
        }

        public async Task<IEnumerable<Champion?>> GetItemsByName(string substring, int index, int count, string? orderingPropertyName = null, bool descending = false)
        {
            IEnumerable<Champion> champs = await Task.Run(() => Context.Champions.ToList().ToPocos().Where(arg => arg.Name.Contains(substring)));
            return SortChampions(champs, index, count, orderingPropertyName, descending);
        }

        public Task<IEnumerable<Champion?>> GetItemsByRunePage(RunePage? runePage, int index, int count, string? orderingPropertyName = null, bool descending = false)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Champion?>> GetItemsBySkill(Skill? skill, int index, int count, string? orderingPropertyName = null, bool descending = false)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Champion?>> GetItemsBySkill(string skill, int index, int count, string? orderingPropertyName = null, bool descending = false)
        {
            throw new NotImplementedException();
        }

        public Task<int> GetNbItemsByCharacteristic(string charName)
        {
            throw new NotImplementedException();
        }

        public Task<int> GetNbItemsByClass(ChampionClass championClass)
        {
            throw new NotImplementedException();
        }

        public async Task<int> GetNbItemsByName(string substring)
        {
            return await Task.FromResult(Context.Champions.Where((arg) => arg.Name.Contains(substring)).Count());
        }

        public Task<int> GetNbItemsByRunePage(RunePage? runePage)
        {
            throw new NotImplementedException();
        }

        public Task<int> GetNbItemsBySkill(Skill? skill)
        {
            throw new NotImplementedException();
        }

        public Task<int> GetNbItemsBySkill(string skill)
        {
            throw new NotImplementedException();
        }

        public async Task<Champion?> UpdateItem(Champion? oldItem, Champion? newItem)
        {
            ChampionEntity? champ = await Context.Champions.SingleOrDefaultAsync((arg) => arg.Name == oldItem.Name);
            if (champ != null)
            {
                champ.Bio = newItem.Bio;
                champ.Name = newItem.Name;
                champ.ChampClass = newItem.Class;
                champ.Icon = newItem.Icon;
                Context.Champions.Update(champ);
                await Context.SaveChangesAsync();
                return champ.ToPoco();
            }
            return null;
        }
    }
}

